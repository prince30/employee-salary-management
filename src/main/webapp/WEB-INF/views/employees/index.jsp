<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employees</title>
</head>
<body>
	<table>
		<tr>
			<th>S/N</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
		<c:forEach var="employee" items="${employees}" varStatus="counter">
			<tr>
				<td>${counter.count}</td>
				<td>${employee.employeeId}</td>
				<td>${employee.name}</td>
				<td>${employee.address}</td>
				<td>${employee.mobile}</td>
				<td>${employee.basicSalary}</td>
				<td>${employee.houseRent}</td>
				<td>${employee.medicalAllowance}</td>
				<td>${employee.totalSalary}</td>
				<td><a href="delete?id=${employee.id}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="create">Add Emplyee</a>
</body>
</html>