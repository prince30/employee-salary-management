<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee Add</title>
</head>
<body>
	<form action="add" method="post">
		<label for="fname">Name:</label><br> 
		<input type="text" name="name" required="required"><br> 
		<label for="fname">Address:</label><br> 
		<input type="text" name="address" required="required"><br> 
		<label for="fname">Mobile:</label><br> 
		<input type="text" name="mobile" required="required"><br> 
		<label for="fname">Basic Salary:</label><br> 
		<input type="text" name="basicSalary"><br> 
		<label for="fname">House Rent:</label><br> 
		<input type="text" name="houseRent" required="required">%<br> 
		<label for="fname">Medical Allowance:</label><br> 
		<input type="text" name="medicalAllowance" required="required">%<br> 
		<label for="fname">Grade:</label><br> 
		<select name="grade" required="required">
	        <option value="6">six</option>
	        <option value="5">five</option>
	        <option value="4">four</option>
	        <option value="3">three</option>
	        <option value="2">two</option>
	        <option value="1">one</option>
		</select>
		<input type="submit" value="Submit">
	</form>
</body>
</html>