<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Grades</title>
</head>
<body>
	<table>
		<tr>
			<th>S/N</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
		<c:forEach var="grade" items="${grades}" varStatus="counter">
			<tr>
				<td>${counter.count}</td>
				<td>${grade.name}</td>
				<td><a href="delete?id=${grade.id}">Delete</a></td>
				<td><a href="edit?id=${grade.id}">Edit</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="create">Add Grade</a>
</body>
</html>