<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Grade</title>
</head>
<body>
	<form action="update" method="post">
	    <input type="hidden" name="id" value="${grade.id}"><br> 
		<label for="fname">Name:</label><br> 
		<input type="text" name="name" value="${grade.name}"><br>  
		<input type="submit" value="save">
	</form>
</body>
</html>