package com.employee.salary.management.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "bank_accounts")
public class BankAccount extends AbstractEntity {

	@NotBlank(message = "Account Name is mandatory")
	private String accountName;
	@NotBlank(message = "Account Number is mandatory")
	private String accountNumber;
	@DecimalMax("10.0") @DecimalMin("0.0") 
	private double currentBalance;
	@NotBlank(message = "Current Balance is mandatory")
	private String bank;
	@NotBlank(message = "Branch is mandatory")
	private String branch;
	@NotBlank(message = "Account Type is mandatory")
	private String accountType;

	public BankAccount() {
		super();
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

}
