package com.employee.salary.management.system.service;

import java.util.List;

import com.employee.salary.management.system.model.Grade;

public interface GradeService {

	Grade save(Grade grade);

	Grade update(Grade grade);

	void delete(Grade grade);

	Grade getById(Long Id);

	List<Grade> getAll();
}
