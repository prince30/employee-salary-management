package com.employee.salary.management.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.employee.salary.management.system.model.Grade;
import com.employee.salary.management.system.service.GradeService;

@Controller
public class GradeController {

	@Autowired
	GradeService gradeService;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create() {
		return "grades/create";
	}

	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String store(@ModelAttribute("grade") Grade grade, ModelMap modelMap) {
		Grade gradeSaveInfo = gradeService.save(grade);
		String msg = "Grade saved with ID : " + gradeSaveInfo.getId();
		modelMap.addAttribute("msg", msg);
		return "grades/create";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		List<Grade> grades = gradeService.getAll();
		model.addAttribute("grades", grades);
		return "grades/index";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(@RequestParam("id") Long id, Model model) {
		Grade grade = gradeService.getById(id);
		gradeService.delete(grade);
		List<Grade> grades = gradeService.getAll();
		model.addAttribute("grades", grades);
		return "grades/index";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(@RequestParam("id") Long id, Model model) {
		Grade grade = gradeService.getById(id);
		model.addAttribute("grade", grade);
		return "grades/edit";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("Grade") Grade grade, Model model) {
		gradeService.update(grade);
		List<Grade> grades = gradeService.getAll();
		model.addAttribute("grades", grades);
		return "grades/index";
	}
}
