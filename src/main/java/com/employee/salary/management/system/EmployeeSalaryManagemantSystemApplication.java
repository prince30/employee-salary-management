package com.employee.salary.management.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeSalaryManagemantSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeSalaryManagemantSystemApplication.class, args);
	}

}
