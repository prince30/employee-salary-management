package com.employee.salary.management.system.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "company_accounts")
public class CompanyAccount extends AbstractEntity {

	
	@OneToOne
	private BankAccount bankAccount;

	public CompanyAccount() {
		super();
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}
}
