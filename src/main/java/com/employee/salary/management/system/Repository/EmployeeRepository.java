package com.employee.salary.management.system.Repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.salary.management.system.model.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
