package com.employee.salary.management.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.salary.management.system.model.Salary;

public interface SalaryRepository extends JpaRepository<Salary, Long> {

}
