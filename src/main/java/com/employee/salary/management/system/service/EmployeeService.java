package com.employee.salary.management.system.service;

import java.util.List;

import com.employee.salary.management.system.model.Employee;

public interface EmployeeService {

	Employee save(Employee employee);

//	Grade update(Grade grade);
//
	void delete(Employee employee);

	Employee getById(Long Id);

	List<Employee> getAll();
}
