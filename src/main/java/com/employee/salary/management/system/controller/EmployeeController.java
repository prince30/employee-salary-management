package com.employee.salary.management.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.employee.salary.management.system.model.Employee;
import com.employee.salary.management.system.model.Grade;
import com.employee.salary.management.system.service.EmployeeService;
import com.employee.salary.management.system.service.GradeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	GradeService gradeService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createEmployee(ModelMap modelMap) {
		List<Grade> grades = gradeService.getAll();
		modelMap.addAttribute("grades", grades);
		return "employees/create";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String storeEmployee(@ModelAttribute("employee") Employee employee) {
		Employee employeeSaveInfo = employeeService.save(employee);
		return "employees/create";
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		List<Employee> employees = employeeService.getAll();
		model.addAttribute("employees", employees);
		return "employees/index";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(@RequestParam("id") Long id, Model model) {
		Employee employee = employeeService.getById(id);
		employeeService.delete(employee);
		List<Employee> employees = employeeService.getAll();
		model.addAttribute("employees", employees);
		return "employees/index";
	}
}
