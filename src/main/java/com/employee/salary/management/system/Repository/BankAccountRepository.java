package com.employee.salary.management.system.Repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.salary.management.system.model.BankAccount;


public interface BankAccountRepository extends JpaRepository<BankAccount, Long>{


}
