package com.employee.salary.management.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "grades")
public class Grade extends AbstractEntity {

	@NotBlank(message = "Name is mandatory")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Grade() {
		super();
	}

}
