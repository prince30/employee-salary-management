package com.employee.salary.management.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "employees")
public class Employee extends AbstractEntity {

	@Column(unique = true)
	private String employeeId;
	@NotBlank(message = "Name is mandatory")
	private String name;
	@NotBlank(message = "Address is mandatory")
	private String address;
	@Range(min = 11, max = 11, message = "Please provide a valid phone number. E.g., 01747267432")
	private String mobile;
	@DecimalMax("10.0") @DecimalMin("0.0") 
	private double basicSalary;
	@DecimalMax("10.0") @DecimalMin("0.0") 
	private double houseRent;
	@DecimalMax("10.0") @DecimalMin("0.0") 
	private double medicalAllowance;
	@DecimalMax("10.0") @DecimalMin("0.0") 
	private double totalSalary;

	@OneToOne
	private Grade grade;

	@OneToOne
	private BankAccount bankAccount;

	public Employee() {
		super();
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public double getHouseRent() {
		return houseRent;
	}

	public void setHouseRent(double houseRent) {
		this.houseRent = houseRent;
	}

	public double getMedicalAllowance() {
		return medicalAllowance;
	}

	public void setMedicalAllowance(double medicalAllowance) {
		this.medicalAllowance = medicalAllowance;
	}

	public double getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(double totalSalary) {
		this.totalSalary = totalSalary;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

}
