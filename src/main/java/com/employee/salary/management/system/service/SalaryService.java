package com.employee.salary.management.system.service;

import com.employee.salary.management.system.model.Salary;

public interface SalaryService {

	Salary save(Salary salary);
}
