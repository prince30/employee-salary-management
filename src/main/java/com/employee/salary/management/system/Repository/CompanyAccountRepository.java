package com.employee.salary.management.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.salary.management.system.model.CompanyAccount;

public interface CompanyAccountRepository extends JpaRepository<CompanyAccount, Long> {

}
