package com.employee.salary.management.system.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.employee.salary.management.system.Repository.GradeRepository;
import com.employee.salary.management.system.model.Grade;

@Service
public class GradeServiceImpl implements GradeService {

	@Autowired
	private GradeRepository gradeRepository;

	@Override
	public Grade save(Grade grade) {
		return gradeRepository.save(grade);
	}

	@Override
	public Grade update(Grade grade) {
		return gradeRepository.save(grade);
	}

	@Override
	public void delete(Grade grade) {
		gradeRepository.delete(grade);
	}

	@Override
	public Grade getById(Long Id) {
		return gradeRepository.getOne(Id);
	}

	@Override
	public List<Grade> getAll() {
		return gradeRepository.findAll();
	}

	public GradeRepository getGradeRepository() {
		return gradeRepository;
	}

	public void setGradeRepository(GradeRepository gradeRepository) {
		this.gradeRepository = gradeRepository;
	}

}
