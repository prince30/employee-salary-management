package com.employee.salary.management.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.employee.salary.management.system.Repository.SalaryRepository;
import com.employee.salary.management.system.model.Salary;

@Service
public class SalaryServiceImpl implements SalaryService {

	@Autowired
	private SalaryRepository salaryRepository;

	@Override
	public Salary save(Salary salary) {
		return salaryRepository.save(salary);
	}
}
