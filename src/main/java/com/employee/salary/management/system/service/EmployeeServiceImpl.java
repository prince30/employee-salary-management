package com.employee.salary.management.system.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.employee.salary.management.system.Repository.BankAccountRepository;
import com.employee.salary.management.system.Repository.EmployeeRepository;
import com.employee.salary.management.system.model.BankAccount;
import com.employee.salary.management.system.model.Employee;


@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private BankAccountRepository bankAccountRepository;

	@Override
	public Employee save(Employee employee) {
		BankAccount bankAccount = new BankAccount();

		bankAccount.setAccountName(employee.getName());
		bankAccount.setAccountNumber(String.valueOf((int) (Math.random() * 90000000) + 10000000));
		bankAccount.setCurrentBalance(0);
		bankAccount.setAccountType("current");
		bankAccount.setBank("DBBL");
		bankAccount.setBranch("Dhaka");

		BankAccount savedAccount = bankAccountRepository.save(bankAccount);
		savedAccount.getId();

		BankAccount bankAccountId = bankAccountRepository.findById(savedAccount.getId()).get();

		Employee emp = new Employee();

		emp.setAddress(employee.getAddress());
		emp.setGrade(employee.getGrade());
		emp.setBasicSalary(employee.getBasicSalary());
		emp.setHouseRent(employee.getHouseRent());
		emp.setMedicalAllowance(employee.getMedicalAllowance());

		emp.setTotalSalary(employee.getBasicSalary() + employee.getHouseRent() * employee.getBasicSalary() * 0.01
				+ employee.getMedicalAllowance() * employee.getBasicSalary() * 0.01);

		emp.setName(employee.getName());
		emp.setMobile(employee.getMobile());
		emp.setEmployeeId(String.valueOf((int) (Math.random() * 9000) + 1000));
		emp.setBankAccount(bankAccountId);
		Employee saveInfo = employeeRepository.save(emp);

		return saveInfo;
	}

	@Override
	public List<Employee> getAll() {
		return employeeRepository.findAll();
	}
	
	@Override
	public void delete(Employee employee) {
		employeeRepository.delete(employee);
	}

	@Override
	public Employee getById(Long Id) {
		return employeeRepository.getOne(Id);
	}
}
