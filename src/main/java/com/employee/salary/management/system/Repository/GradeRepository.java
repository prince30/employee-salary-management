package com.employee.salary.management.system.Repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.salary.management.system.model.Grade;


public interface GradeRepository extends JpaRepository<Grade, Long>{

}
