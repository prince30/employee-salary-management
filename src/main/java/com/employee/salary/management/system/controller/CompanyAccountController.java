package com.employee.salary.management.system.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.employee.salary.management.system.Repository.BankAccountRepository;
import com.employee.salary.management.system.Repository.SalaryRepository;
import com.employee.salary.management.system.model.BankAccount;

@Controller
public class CompanyAccountController {

	@Autowired
	BankAccountRepository bankAccountRepository;

	@Autowired
	SalaryRepository salaryRepository;


	@RequestMapping(value = "/company/details", method = RequestMethod.GET)
	public String create() {
		return "companies/details";
	}

	@RequestMapping(value = "/addBalance", method = RequestMethod.GET)
	public String addBalance() {
		return "companies/addBalance";
	}

	@RequestMapping(value = "/addBalance", method = RequestMethod.POST)
	public String storeBalance(@RequestParam("bankAccountId") Long bankAccountId, Long currentBalance,
			ModelMap modelMap) {
		BankAccount bankAccount = bankAccountRepository.getOne(bankAccountId);
		double balance = bankAccount.getCurrentBalance();
		double totalBalance = balance + currentBalance;
		bankAccount.setCurrentBalance(totalBalance);
		bankAccountRepository.save(bankAccount);
		return "companies/addBalance";
	}

	@RequestMapping(value = "/currentBalance", method = RequestMethod.GET)
	public String currentBalance(ModelMap modelMap) {
		BankAccount bankAccount = bankAccountRepository.getOne((long) 1);
		double currentBalance = bankAccount.getCurrentBalance();
		modelMap.addAttribute("currentBalance", currentBalance);
		return "companies/currentBalance";
	}
}
